var data = {
    en: {
        homepageNav: "Homepage",
        sellNav: "Sell",
    },
    vi: {
        homepageNav: "Trang chủ",
        sellNav: "Bán",
    }
};

function engLang() {
    // setTimeout(function () {
    if (window.location.hash === '#en') {
        homepageNav.textContent = data.en.homepageNav;
        sell1Nav.textContent = data.en.sellNav;
    }
    // }, 1)
}

function vietLang() {
    // setTimeout(function () {
    if (window.location.hash === '#vi') {
        homepageNav.textContent = data.vi.homepageNav;
        sell1Nav.textContent = data.vi.sellNav;
        // }, 1)
    }
}
